﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace SNOMEDFilter
{
    class Program
    {
        static SqlConnection connection;

        static void Main(string[] args)
        {
            connection = new SqlConnection();
            connection.ConnectionString = "data source=.; initial catalog=SNOMED_CT; integrated security=true;";

            Queue<Concept> queue = new Queue<Concept>();
            int counter=0;
            foreach (Int64 conceptID in GetBaseBodyStrucutreConcepts())
            {
                Console.WriteLine(counter++);
                foreach (Concept concept in GetConceptChildren(conceptID))
                {   
                    queue.Enqueue(concept);


                }
            }

            



            while (queue.Count != 0)
            {
                Console.WriteLine(queue.Count);

                bool insterted = false;
                Concept concept = queue.Dequeue();
                Int64 conceptID = concept.ConceptID;

                if (!ExistsInDatabase(conceptID))
                {
                    if (GetConceptDisorderConcepts(conceptID).Count != 0 || GetConceptFindings(conceptID).Count != 0)
                    {
                        InsertBodyStrucutreConcept(concept);
                        insterted = true;
                        Console.WriteLine("...");
                    }

                    List<Concept> childConcepts = GetConceptChildren(concept.ConceptID);
                    if (childConcepts.Count > 0)
                    {
                        if (!insterted)
                        {
                            InsertBodyStrucutreConcept(concept);
                            Console.WriteLine("...");
                        }

                        foreach (Concept childConcept in childConcepts)
                            if(!queue.Contains(childConcept))
                                queue.Enqueue(childConcept);


                    }

                }

            }



        }

        private static bool ExistsInDatabase(long conceptID)
        {
            bool Isthere = false;
            SqlCommand command = connection.CreateCommand();
            command.CommandText = "select conceptID from core.bodyStructure where conceptID=@conceptID";
            command.Parameters.AddWithValue("@conceptID", conceptID);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                Isthere = true;

            }
            connection.Close();
            return Isthere;
        }

        static List<Concept> GetConceptChildren(Int64 conceptID)
        {
            List<Concept> childConcepts = new List<Concept>();
            SqlCommand command = connection.CreateCommand();
            command.CommandText = "select sourceID, SUBSTRING(sourcename, 0, charindex('(', sourcename,0)) from dbo.vw_ConceptRelationships where TargetID = @conceptID and sourceID not in (select conceptID from core.bodyStructure) and sourcetype='(body structure)' and relationshipName in ('Is a (attribute)', 'part of (attribute)') and sourcename not like 'Entire%'";
            command.Parameters.AddWithValue("@conceptID", conceptID);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Concept concept = new Concept() { ConceptID = reader.GetInt64(0), FullName = reader.GetString(1) };
                childConcepts.Add(concept);

            }
            connection.Close();
            return childConcepts;
        }

        static List<Concept> GetConceptFindings(Int64 conceptID)
        {
            List<Concept> findingConcepts = new List<Concept>();
            SqlCommand command = connection.CreateCommand();
            command.CommandText = "select  distinct sourceID, SUBSTRING(sourcename, 0, charindex('(', sourcename,0)) from dbo.vw_ConceptRelationships where TargetID =@conceptID and relationshipName= 'finding site (attribute)' and sourcetype='(finding)'";
            command.Parameters.AddWithValue("@conceptID", conceptID);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Concept concept = new Concept() { ConceptID = reader.GetInt64(0), FullName = reader.GetString(1) };
                findingConcepts.Add(concept);

            }
            connection.Close();
            return findingConcepts;
        }

        static List<Concept> GetConceptDisorderConcepts(Int64 conceptID)
        {
            List<Concept> disorderConcepts = new List<Concept>();
            SqlCommand command = connection.CreateCommand();
            command.CommandText = "select  distinct sourceID, SUBSTRING(sourcename, 0, charindex('(', sourcename,0)) from dbo.vw_ConceptRelationships where TargetID=@conceptID and relationshipName='finding site (attribute)' and sourcetype='(disorder)'";
            command.Parameters.AddWithValue("@conceptID", conceptID);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Concept concept = new Concept() { ConceptID = reader.GetInt64(0), FullName = reader.GetString(1) };
                disorderConcepts.Add(concept);

            }
            connection.Close();
            return disorderConcepts;
        }

        static void InsertBodyStrucutreConcept(Concept concept)
        {
            SqlCommand command = connection.CreateCommand();
            command.CommandText = "insert into core.BodyStructure (ConceptID,FullName) values(@ConceptID,@FullName)";
            command.Parameters.AddWithValue("@ConceptID", concept.ConceptID);
            command.Parameters.AddWithValue("@FullName", concept.FullName);
            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();
        }

        //static void InsertFindingeConcept(Concept concept)
        //{
        //    SqlCommand command = connection.CreateCommand();
        //    command.CommandText = "insert into core.Finding (ConceptID,FullName) values(@ConceptID,@FullName)";
        //    command.Parameters.AddWithValue("@ConceptID", concept.ConceptID);
        //    command.Parameters.AddWithValue("@FullName", concept.FullName);
        //    connection.Open();
        //    command.ExecuteNonQuery();
        //    connection.Close();
        //}

        //static void InsertDisorderConcept(Concept concept)
        //{
        //    SqlCommand command = connection.CreateCommand();
        //    command.CommandText = "insert into core.Disorder (ConceptID,FullName) values(@ConceptID,@FullName)";
        //    command.Parameters.AddWithValue("@ConceptID", concept.ConceptID);
        //    command.Parameters.AddWithValue("@FullName", concept.FullName);
        //    connection.Open();
        //    command.ExecuteNonQuery();
        //    connection.Close();
        //}

        static List<Int64> GetBaseBodyStrucutreConcepts()
        {
            List<Int64> concepts = new List<Int64>();
            SqlCommand command = connection.CreateCommand();
            command.CommandText = "select ConceptID from core.BodyStructure";
            //command.CommandText = "select distinct targetID from dbo.vw_ConceptRelationships where targetID in (select conceptID from core.bodyStructure) and sourceID not in (select conceptID from core.bodyStructure) and relationshipName in ('Is a (attribute)', 'part of (attribute)') and sourcetype = '(body structure)' and sourcename not like 'Entire%'";
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                concepts.Add(reader.GetInt64(0));

            }
            connection.Close();
            return concepts;
        }

    }

    public class Concept
    {
        public Int64 ConceptID
        {
            get;
            set;
        }

        public string FullName
        {
            get;
            set;
        }


    }
}
