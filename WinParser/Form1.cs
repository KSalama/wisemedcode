﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinParser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }



        System.IO.StreamReader reader;

        private void button1_Click(object sender, EventArgs e)
        {
            this.textBox2.Clear();

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                reader = new System.IO.StreamReader(this.openFileDialog1.OpenFile());
                textBox1.Text = this.openFileDialog1.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string line = reader.ReadLine();
            this.textBox2.Text += line;
            this.textBox2.Text += Environment.NewLine;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection())
            {
                connection.ConnectionString = "data source=.; initial catalog=SNOMED_CT; integrated security=true;";
                System.Data.SqlClient.SqlCommand command=connection.CreateCommand();
                command.CommandText = "insert into core.concept values (@conceptID,@status,@Fullname,@type,@CTV3ID,@SNOMEDID,@IsPrimitive)";
                connection.Open();

                reader.ReadLine();
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] parts = line.Split('\t');
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@conceptID", Int64.Parse(parts[0]));
                    command.Parameters.AddWithValue("@status", int.Parse(parts[1]));

                    //string fullname = parts[2].Contains("(") ? parts[2].Substring(0, parts[2].LastIndexOf("(")) : parts[2];
                    //string type = parts[2].Contains(")") ? parts[2].Substring(parts[2].LastIndexOf("(") + 1, parts[2].Length - parts[2].LastIndexOf("(") - 2) : "NA";

                    
                    string fullname = parts[2];
                    
                    string type = "NA";
                    
                    
                    command.Parameters.AddWithValue("@FullName", fullname);
                    command.Parameters.AddWithValue("@type", type);
                    command.Parameters.AddWithValue("@CTV3ID", parts[3]);
                    command.Parameters.AddWithValue("@SNOMEDID", parts[4]);
                    command.Parameters.AddWithValue("@IsPrimitive", int.Parse(parts[5]));

                    command.ExecuteNonQuery();
                    

                }
            }

            MessageBox.Show("Done");
        
        }

        private void button4_Click(object sender, EventArgs e)
        {
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection())
            {
                connection.ConnectionString = "data source=.; initial catalog=SNOMED_CT; integrated security=true;";
                System.Data.SqlClient.SqlCommand command = connection.CreateCommand();
                command.CommandText = "insert into core.description values (@descriptionID,@status,@ConceptID,@Term,@Initial,@DescType,@LangCode)";
                connection.Open();

                reader.ReadLine();
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] parts = line.Split('\t');
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@descriptionID", Int64.Parse(parts[0]));
                    command.Parameters.AddWithValue("@status", int.Parse(parts[1]));
                    command.Parameters.AddWithValue("@ConceptID", Int64.Parse(parts[2]));
                    command.Parameters.AddWithValue("@Term", parts[3]);
                    command.Parameters.AddWithValue("@Initial",  int.Parse( parts[4]));
                    command.Parameters.AddWithValue("@DescType", int.Parse(parts[5]));
                    command.Parameters.AddWithValue("@LangCode", parts[6]);

                    command.ExecuteNonQuery();


                }
            }

            MessageBox.Show("Done");
        
        }
    }
}
