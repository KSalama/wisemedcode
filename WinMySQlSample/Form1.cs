﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace WinMySQlSample
{
    public partial class Form1 : Form
    {

        MySqlConnection mySqlConnection;
        SqlConnection sqlConnection;


        public Form1()
        {
            InitializeComponent();


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            mySqlConnection = new MySqlConnection();
            sqlConnection = new System.Data.SqlClient.SqlConnection("data source=.; initial catalog=SNOMED_CT; integrated security=true;");

            mySqlConnection.ConnectionString = "Server=wisemed.c85bwzay5dit.us-east-1.rds.amazonaws.com; " +
           "database=wisemed; " +
           "uid=root; " +
           "password=WiseMed123;";
            mySqlConnection.Open();
            MessageBox.Show(mySqlConnection.State.ToString());


        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            mySqlConnection.Clone();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                MySqlCommand command = mySqlConnection.CreateCommand();
                command.CommandText = textBox1.Text;
                command.ExecuteNonQuery();
                MessageBox.Show("Done");
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + Environment.NewLine + ex.InnerException);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                MySqlCommand command = mySqlConnection.CreateCommand();
                command.CommandText = textBox1.Text;
                MySqlDataReader reader= command.ExecuteReader();
                DataTable tb = new DataTable();
                tb.Load(reader);
                reader.Close();
                this.dataGridView1.DataSource = tb;

                 

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + Environment.NewLine + ex.InnerException);
            }

        }
    }
}
