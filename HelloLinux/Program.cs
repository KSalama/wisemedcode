﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelloLinux
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello Linux, I am .NET!");
            if (args.Length > 0)
            {
                try
                {
                    int x = int.Parse(args[0]);
                    for (int i = 0; i < x; i++)
                        Console.WriteLine(i.ToString());
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
