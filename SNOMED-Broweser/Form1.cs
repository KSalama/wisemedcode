﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace SNOMED_Broweser
{
    public partial class Form1 : Form
    {
              
        MySqlConnection mySqlConnection;
        

      

        public Form1()
        {
            InitializeComponent();
            mySqlConnection = new MySqlConnection();
           
            mySqlConnection.ConnectionString = "Server=wisemed.c85bwzay5dit.us-east-1.rds.amazonaws.com; " +
           "database=wisemed; " +
           "uid=root; " +
           "password=WiseMed123;";

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            this.mySqlConnection.Open();
            this.Text += " - Connected";
            this.tvConcepts.Nodes.Add("91723000", "Body Strucutre - Anatomical Structure");
            this.tvConcepts.SelectedNode = this.tvConcepts.TopNode;
            
        }

        private void tvConcepts_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //this.lstTerms.Items.Clear();
            this.lstFindings.Items.Clear();
            this.lstDisorders.Items.Clear();
            

            string conceptID = this.tvConcepts.SelectedNode.Name;
            //this.GetConceptInfo(conceptID);
            this.ExtendTree(e.Node);
            this.GetConceptRelatedRelationships(conceptID);

        }

        private void ExtendTree(TreeNode node)
        {
            string conceptID = node.Name;            
            if (node.Nodes.Count != 0)
                return;
            

            MySqlCommand command1 = mySqlConnection.CreateCommand();
            command1.CommandText = "select conceptID, fullname from BodyStructure where conceptID in (select sourceConceptID from Relationship where TargetConceptID = @conceptID) order by fullname";
            command1.Parameters.AddWithValue("@conceptID", conceptID);

      
            MySqlDataReader reader = command1.ExecuteReader();
            while (reader.Read())
            {
                string childConceptID = reader.GetInt64(0).ToString();
                string fullname=reader.GetString(1);
                node.Nodes.Add(childConceptID, fullname);
            }

            reader.Close();
          
            
        }

        //private void GetConceptInfo(string conceptID)
        //{

        //    MySqlCommand command1 = mySqlConnection.CreateCommand();
        //    command1.CommandText = "select fullname from BodyStructure where conceptID=@conceptID";
        //    command1.Parameters.AddWithValue("@conceptID", conceptID);

        //    MySqlCommand command2 = mySqlConnection.CreateCommand();
        //    command2.CommandText = "select term from Description where conceptID=@conceptID";
        //    command2.Parameters.AddWithValue("@conceptID", conceptID);


        //    MySqlDataReader reader1 = command1.ExecuteReader(CommandBehavior.SingleRow);
        //    if (reader1.Read())
        //        this.lbConceptFullName.Text = reader1.GetString(0);
        //    reader1.Close();

        //    MySqlDataReader reader2 = command2.ExecuteReader();
        //    while (reader2.Read())
        //    {
        //        lstTerms.Items.Add(reader2.GetString(0));
        //    }

        //    reader2.Close();
        
        //}


        private void GetConceptRelatedRelationships(string conceptID)
        {
            this.lstDisorders.Items.Clear();
            this.lstFindings.Items.Clear();

            MySqlCommand command1 = mySqlConnection.CreateCommand();
            command1.CommandText = "select distinct fullname from Finding where conceptID in (select SourceConceptID from Relationship where TargetConceptID=@conceptID) order by fullname";
            command1.Parameters.AddWithValue("@conceptID", conceptID);
            

            MySqlDataReader reader = command1.ExecuteReader();
            while (reader.Read())
            {
                string findingName = reader.GetString(0);
                this.lstFindings.Items.Add(findingName);


            }

            reader.Close();

            
            MySqlCommand command2 = mySqlConnection.CreateCommand();
            command2.CommandText = "select distinct fullname from Disorder where conceptID in (select SourceConceptID from Relationship where TargetConceptID=@conceptID) order by fullname";
            command2.Parameters.AddWithValue("@conceptID", conceptID);
            

            MySqlDataReader reader2 = command2.ExecuteReader();
            while (reader2.Read())
            {
                string disorderName = reader2.GetString(0);
                this.lstDisorders.Items.Add(disorderName);


            }

            reader2.Close();
 
        }


        

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            mySqlConnection.Close();
        }

  
    }
}
