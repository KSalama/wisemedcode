﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace MySqlUtility
{
    class Program
    {
        static void Main(string[] args)
        {
            //LoadDescription();

            //LoadConcept();
            
            //LoadFinding();

            //LoadDisorder();

            LoadDescription();
            
            
        }

        static void LoadConcept()
        {
            MySqlConnection mySqlConnection;
            SqlConnection sqlConnection;

            mySqlConnection = new MySqlConnection();
            sqlConnection = new System.Data.SqlClient.SqlConnection("data source=.; initial catalog=SNOMED_CT; integrated security=true;");

            mySqlConnection.ConnectionString = "Server=wisemed.c85bwzay5dit.us-east-1.rds.amazonaws.com; " +
           "database=wisemed; " +
           "uid=root; " +
           "password=WiseMed123;";


            try
            {
                SqlCommand source = sqlConnection.CreateCommand();
                source.CommandText = "Select conceptID, fullname from core.Concept";

                MySqlCommand destination = mySqlConnection.CreateCommand();
                destination.CommandText = "insert into Concept values (@conceptID, @FullName)";


                sqlConnection.Open();
                mySqlConnection.Open();

                Console.WriteLine("Connections are OK");

                SqlDataReader reader = source.ExecuteReader();
                int counter = 0;
                while (reader.Read())
                {
                    destination.Parameters.Clear();
                    destination.Parameters.AddWithValue("@conceptID", reader.GetInt64(0));
                    destination.Parameters.AddWithValue("@fullName", reader.GetString(1));

                    counter += destination.ExecuteNonQuery();

                    Console.WriteLine(counter);
                }

                Console.WriteLine("Done");


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.InnerException);
            }
            finally
            {
                sqlConnection.Close();
                mySqlConnection.Close();
            }


        }

        static void LoadBodyStrucutre()
        {
              MySqlConnection mySqlConnection;
            SqlConnection sqlConnection;

            mySqlConnection = new MySqlConnection();
            sqlConnection = new System.Data.SqlClient.SqlConnection("data source=.; initial catalog=SNOMED_CT; integrated security=true;");

            mySqlConnection.ConnectionString = "Server=wisemed.c85bwzay5dit.us-east-1.rds.amazonaws.com; " +
           "database=wisemed; " +
           "uid=root; " +
           "password=WiseMed123;";


            try
            {
                SqlCommand source = sqlConnection.CreateCommand();
                source.CommandText = "Select conceptID, fullname from core.BodyStrucutre";

                MySqlCommand destination = mySqlConnection.CreateCommand();
                destination.CommandText = "insert into BodyStructure values (@conceptID, @FullName)";


                sqlConnection.Open();
                mySqlConnection.Open();

                Console.WriteLine("Connections are OK");

                SqlDataReader reader = source.ExecuteReader();
                int counter=0;
                while (reader.Read())
                {
                    destination.Parameters.Clear();
                    destination.Parameters.AddWithValue("@conceptID", reader.GetInt64(0));
                    destination.Parameters.AddWithValue("@fullName", reader.GetString(1));
                    
                    counter+= destination.ExecuteNonQuery();

                    Console.WriteLine(counter);
                }

                Console.WriteLine("Done");

 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.InnerException);
            }
            finally
            {
                sqlConnection.Close();
                mySqlConnection.Close();
            }
        
 
        }

        static void LoadFinding()
        {
            MySqlConnection mySqlConnection;
            SqlConnection sqlConnection;

            mySqlConnection = new MySqlConnection();
            sqlConnection = new System.Data.SqlClient.SqlConnection("data source=.; initial catalog=SNOMED_CT; integrated security=true;");

            mySqlConnection.ConnectionString = "Server=wisemed.c85bwzay5dit.us-east-1.rds.amazonaws.com; " +
           "database=wisemed; " +
           "uid=root; " +
           "password=WiseMed123;";


            try
            {
                SqlCommand source = sqlConnection.CreateCommand();
                source.CommandText = "Select conceptID, fullname from core.Finding";
                MySqlCommand destination = mySqlConnection.CreateCommand();
                destination.CommandText = "insert into Finding values (@conceptID, @FullName)";



                sqlConnection.Open();
                mySqlConnection.Open();

                Console.WriteLine("Connections are OK");

                SqlDataReader reader = source.ExecuteReader();
                int counter = 0;
                while (reader.Read())
                {
                   
                    destination.Parameters.Clear();
                    destination.Parameters.AddWithValue("@conceptID", reader.GetInt64(0));
                    destination.Parameters.AddWithValue("@fullName", reader.GetString(1));
                    
                    counter += destination.ExecuteNonQuery();

                    Console.WriteLine(counter);
                }

                Console.WriteLine("Done");


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.InnerException);
            }
            finally
            {
                sqlConnection.Close();
                mySqlConnection.Close();
            }

        }

        static void LoadDisorder()
        {

            MySqlConnection mySqlConnection;
            SqlConnection sqlConnection;

            mySqlConnection = new MySqlConnection();
            sqlConnection = new System.Data.SqlClient.SqlConnection("data source=.; initial catalog=SNOMED_CT; integrated security=true;");

            mySqlConnection.ConnectionString = "Server=wisemed.c85bwzay5dit.us-east-1.rds.amazonaws.com; " +
           "database=wisemed; " +
           "uid=root; " +
           "password=WiseMed123;";


            try
            {
                SqlCommand source = sqlConnection.CreateCommand();
                source.CommandText = "Select conceptID, fullname from core.Disorder";

                MySqlCommand destination = mySqlConnection.CreateCommand();
                destination.CommandText = "insert into Disorder values (@conceptID, @FullName)";

                sqlConnection.Open();
                mySqlConnection.Open();

                Console.WriteLine("Connections are OK");

                SqlDataReader reader = source.ExecuteReader();
                int counter = 0;
                while (reader.Read())
                {

                    destination.Parameters.Clear();
                    destination.Parameters.AddWithValue("@conceptID", reader.GetInt64(0));
                    destination.Parameters.AddWithValue("@fullName", reader.GetString(1));

                    counter += destination.ExecuteNonQuery();

                    Console.WriteLine(counter);
                }

                Console.WriteLine("Done");


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.InnerException);
            }
            finally
            {
                sqlConnection.Close();
                mySqlConnection.Close();
            }


        }

        static void LoadDescription()
        {

            MySqlConnection mySqlConnection;
            SqlConnection sqlConnection;

            mySqlConnection = new MySqlConnection();
            sqlConnection = new System.Data.SqlClient.SqlConnection("data source=.; initial catalog=SNOMED_CT; integrated security=true;");

            mySqlConnection.ConnectionString = "Server=wisemed.c85bwzay5dit.us-east-1.rds.amazonaws.com; " +
           "database=wisemed; " +
           "uid=root; " +
           "password=WiseMed123;";


            try
            {
                SqlCommand source = sqlConnection.CreateCommand();
                source.CommandText = "Select DescriptionID, Status, conceptID, Term from core.Description"+
               " where ConceptID in (select ConceptID from core.BodyStructure)";

                MySqlCommand destination = mySqlConnection.CreateCommand();
                destination.CommandText = "insert into Description values (@descriptionID, @status, @ConceptID, @term)";

                sqlConnection.Open();
                mySqlConnection.Open();

                Console.WriteLine("Connections are OK");

                SqlDataReader reader = source.ExecuteReader();
                int counter = 0;
                while (reader.Read())
                {

                    destination.Parameters.Clear();
                    destination.Parameters.AddWithValue("@descriptionID", reader.GetInt64(0));
                    destination.Parameters.AddWithValue("@status", reader.GetInt16(1));
                    destination.Parameters.AddWithValue("@ConceptID", reader.GetInt64(2));
                    destination.Parameters.AddWithValue("@term", reader.GetString(3));

                    counter += destination.ExecuteNonQuery();

                    Console.WriteLine(counter);
                }

                Console.WriteLine("Done");


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.InnerException);
                Console.ReadLine();
            }
            finally
            {
                sqlConnection.Close();
                mySqlConnection.Close();
            }


        }

        static void LoadRelationships()
        {

            MySqlConnection mySqlConnection;
            SqlConnection sqlConnection;

            mySqlConnection = new MySqlConnection();
            sqlConnection = new System.Data.SqlClient.SqlConnection("data source=.; initial catalog=SNOMED_CT; integrated security=true;");

            mySqlConnection.ConnectionString = "Server=wisemed.c85bwzay5dit.us-east-1.rds.amazonaws.com; " +
           "database=wisemed; " +
           "uid=root; " +
           "password=WiseMed123;";


            try
            {
                SqlCommand source = sqlConnection.CreateCommand();
                source.CommandText = "Select RelationshipID, SourceConceptID, RelationshipConceptID, TaregetConceptID from core.Relationship";

                MySqlCommand destination = mySqlConnection.CreateCommand();
                destination.CommandText = "insert into Relationship values (@RelationshipID, @SourceConceptID, @RelationshipConceptID, @TaregtConceptID)";

                sqlConnection.Open();
                mySqlConnection.Open();

                Console.WriteLine("Connections are OK");

                SqlDataReader reader = source.ExecuteReader();
                int counter = 0;
                while (reader.Read())
                {

                    destination.Parameters.Clear();
                    destination.Parameters.AddWithValue("@RelationshipID", reader.GetInt64(0));
                    destination.Parameters.AddWithValue("@SourceConceptID", reader.GetInt64(1));
                    destination.Parameters.AddWithValue("@RelationshipConceptID", reader.GetInt64(2));
                    destination.Parameters.AddWithValue("@TaregtConceptID", reader.GetInt64(3));

                    counter += destination.ExecuteNonQuery();

                    Console.WriteLine(counter);
                }

                Console.WriteLine("Done");


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.InnerException);
            }
            finally
            {
                sqlConnection.Close();
                mySqlConnection.Close();
            }


        }
    }
}
